﻿
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '@/_models';
import { Customer } from '@/_models/customer';
import { AlertService, UserService, AuthenticationService } from '@/_services';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {

    mobNumberPattern = "^((\\+91-?)|0)?[0-9]{10}$";  
    customerForm: FormGroup;
    currentUser: User;
    currentUserSubscription: Subscription;
    users: User[] = [];
    Cusomer: Customer[] =[];
    submitted = false;
    loading = false;
    

    constructor(
        private formBuilder: FormBuilder,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
        private router: Router
    ) {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

   

    

    ngOnInit() {
        this.loadAllUsers();
        this.customerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            DOB: ['', Validators.required],
            streetAddress: ['', Validators.required],
            city: ['', Validators.required],
            state:['', Validators.required],
            zipcode:['', Validators.required],
            email:['', Validators.required],
            sex:['', Validators.required],
            // lastLogin: ['', Validators.required],
            // password:['', Validators.required],
            contactNumber:['', Validators.required],

            // password: ['', [Validators.required, Validators.minLength(6)]]
        });
        
    }

    // convenience getter for easy access to form fields
    get f() { return this.customerForm.controls; }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
    
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
        }
      }
      

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
    }

    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => {
            this.loadAllUsers()
        });
    }

    private loadAllUsers() {
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.users = users;
        });
    }

    onSubmit() {
        this.submitted = true;
        console.log(this.customerForm.value);
        console.log("test1");
        
        if (this.customerForm.invalid) {
            return;
        }
        
        this.loading = true;
        this.userService.register1(this.customerForm.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    console.log("data inside");
                    
                    this.router.navigate(['/customerData']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
    
}