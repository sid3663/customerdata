export class Account {
    id: number;
    accountBalance: number;
    branchID:number;
    dateOpened: number;
    accountTypeID: number;
    status: boolean;
}