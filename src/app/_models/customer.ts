export class Customer {
    id: number;
    firstName: string;
    lastName:string;
    DOB: number;
    streetAddress: string;
    city: string;
    state:string;
    zipcode:string;
    email:string;
    sex:string;
    lastLogin: number;
    password:string;
    contactNumber:string;
}